// import Product from './images/Product.jpg';
// <img src={Product} className="App-logo" alt="logo" />
import './App.css';
import Container from 'react-bootstrap/Container'
import { useState, useEffect } from 'react'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2'

import { UserProvider } from './UserContext'  
//UserProvider will serve as an access gate to the UserContext  

import AppNavbar from './components/AppNavbar'
import ProductView from './components/ProductView'
import ActiveProducts from './components/ActiveProducts'
import AllProducts from './components/AllProducts'
import CreateProduct from './components/CreateProduct'
import ProductUpdate from './components/ProductUpdate'
import OrderHistory from './components/OrderHistory'
import OrderInfo from './components/OrderInfo'
import ChangePassword from './components/ChangePassword';
import UserInfo from './components/UserNewUpdate';

import Home from './pages/Home'
import Cart from './pages/Cart'
import Profile from './pages/Profile'
import Logout from './pages/Logout'
import Login from './pages/Login'
import Products from './pages/Products'
import Orders from './pages/Orders'



function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    firstName: null
  })

  const [search, setSearch] = useState('');
  const [source, setSource] = useState('All');

  const [cartBadge, setCartBadge] = useState(0);
  let badge = []
    //cart Badge

  const [cart, setCart] = useState([]);
  //for cart array

  const [orders, setOrders] = useState([]);
  //for order array

  const [products, setProduct] = useState([]);
  // for product list


  const [refreshCart, setRefreshCart] = useState(true)

  const updateCart = (value) => {
    setRefreshCart(value)
  }

  const [productInfo, setProductInfo] = useState({
    pID:'',
    productName: '',
    productCategory: '',
    productVolume: '',
    productDescription: '',
    productPrice: ''
  });

  const unsetUser = () => {
    Swal.fire({
      title: "Logout",
      icon: "success",
      text: "See you soon!",
      showConfirmButton: false,
      timer: 1000
    })
    localStorage.clear();
  }

  useEffect(() => {

    if ((user.id === null) && (getToken !== null)){
      retrieveUserDetails(getToken)
      setGetToken(null)
    }

  }, [user])




  const [getToken, setGetToken] = useState(localStorage.getItem('token'))

  const retrieveUserDetails = (token) => {
        fetch('https://stark-refuge-17906.herokuapp.com/api/users/details', {
            headers: {
                Authorization :`Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                firstName:data.firstName
               
            });

            data.cart.forEach((item,idx) => {
                if(item.isCheckedOut === false){
                    badge.push(item.productId)
                }
              })

            //#of items
            setCartBadge(badge.length)

        })
    }

  const retrieveProductDetails = (productId) => {

    fetch(`https://stark-refuge-17906.herokuapp.com/api/products/${productId}`)
    .then(response => response.json())
    .then(data => {
        setProductInfo({
          pID:data._id,
          productName: data.productName,
          productCategory: data.productCategory,
          productVolume: data.productVolume,
          productDescription: data.productDescription,
          productPrice: data.productPrice
        });
    })
  }  


  
  return (
    <UserProvider value={{user, setUser, unsetUser, retrieveUserDetails, products, setProduct, retrieveProductDetails, productInfo, setProductInfo, cart, setCart, orders, setOrders, updateCart, refreshCart, cartBadge, setCartBadge, search, setSearch, source, setSource}} >
      <Router >
      <AppNavbar />
      <div>
          <Switch>
            <Route exact path ="/" component={Home} />
            <Route exact path ="/logout" component={Logout} />
            <Route exact path ="/login" component={Login} />
            <Route exact path ="/register" component={UserInfo} />

            <Route exact path ="/profile" component={Profile} />
            <Route exact path ="/profile/changePassword" component={ChangePassword} />
            <Route exact path ="/profile/updateInfo" component={UserInfo} />

            <Route exact path ="/cart" component={Cart} />
            <Route exact path ="/orderHistory" component={OrderHistory} />

            <Route exact path ="/products" component={Products} />
            <Route exact path ="/products/:productId" component={ProductView} />
            <Route exact path ="/products/active" component={ActiveProducts} />


            <Route exact path ="/orders" component={Orders} />
            <Route exact path ="/orders/:orderId" component={OrderInfo} />
            <Route exact path ="/createProduct" component={CreateProduct} />
            <Route exact path ="/products/:productId/update" component={ProductUpdate} />

            <Redirect to='/' />
          </Switch>
      </div>

      </Router>
    </UserProvider>
  );
}

export default App;
