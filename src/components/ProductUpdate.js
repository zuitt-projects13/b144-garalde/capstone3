import { useState, useEffect,useContext } from 'react'
import UserContext from '../UserContext'  

import { Container, Form, Button, InputGroup } from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Redirect, useParams, useHistory,} from 'react-router-dom'




export default function ProductUpdate() {

	const history = useHistory()

	const [productName, setProductName] = useState('');
	const [productCategory, setProductCategory] = useState('');
	const [productVolume, setProductVolume] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState('');
	const [isActive, setIsActive] = useState(false);

	const { productId } = useParams();

	const { user, retrieveProductDetails, productInfo, setProductInfo } = useContext(UserContext)

	useEffect(() => {
		if(productName !== '' && productCategory !== 'Select category' && productVolume !== '' && productDescription !== '' && productPrice > 0){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [productName, productCategory, productVolume, productDescription, productPrice])


	useEffect(() => {
		if(productInfo.productName === ''|| productInfo.pID !== productId){
			retrieveProductDetails(productId)
		}
		setProductName(productInfo.productName);
	    setProductCategory(productInfo.productCategory);
	    setProductVolume(productInfo.productVolume);
	    setProductDescription(productInfo.productDescription);
	    setProductPrice(productInfo.productPrice);

	},[productId,productInfo]);


		function updateProduct(e) {
			e.preventDefault();



			fetch(`https://stark-refuge-17906.herokuapp.com/api/products/${productId}/update`,{
				method:'PUT',
				headers: {
					"Content-Type":"application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productName: productName,
					productCategory: productCategory,
					productVolume: productVolume,
					productDescription: productDescription,
					productPrice: productPrice
				})
			})
			.then(response => response.json())
			.then(data => {
				if (data === false){
	        		Swal.fire({
	                  title: "Error",
	                  icon: "error",
	                  text: "An error has been encountered while saving."
	                })
				}
				else {
					Swal.fire({
			          title: "Saved",
			          icon: "success",
			          showConfirmButton: false,
			          timer: 1000
			        })

					backToProducts()
					
				}
			})	
		}

	const backToProducts = () => {

		setProductName('')
		setProductCategory('')
		setProductVolume('')
		setProductDescription('')
		setProductPrice('')

		setProductInfo({
			pID:'',
			productName: '',
			productCategory: '',
			productVolume: '',
			productDescription: '',
			productPrice: ''
		})


		history.push("/products")
	}	

	return (
		(user.id === null || user.isAdmin === false ) ?
            <Redirect to="/" />
        :
			<Container className="formContainer mt-3 p-4 w-75">
				<h1>Update Product</h1>
				
				<Form className="mt-3" onSubmit={(e) => updateProduct(e)}>
				  	<Form.Group className="mb-3" controlId="productName">
				    <Form.Label>Product Name</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter product name" 
				    	value = {productName}
				    	onChange = { e => setProductName(e.target.value)}
				    	required 
				    />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="productCategory">
				    <Form.Label>Category</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				   
				    <Form.Select 
				    	value={productCategory}
				    	onChange = { e => setProductCategory(e.target.value)}
				    	required
				    >
				      <option>Select category</option>
				      <option value="Shampoo">Shampoo</option>
				      <option value="Conditioner">Conditioner</option>
				      <option value="Styler">Styler</option>
				    </Form.Select>
				    </Form.Group>



				    <Form.Group className="mb-3" controlId="productVolume">
				    <Form.Label>Volume</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter volume" 
				    	value = {productVolume}
				    	onChange = { e => setProductVolume(e.target.value)}
				    	required 
				    />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="productDescription">
				    <Form.Label>Description</Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter description" 
				    	value = {productDescription}
				    	onChange = { e => setProductDescription(e.target.value)}
				    	required 
				    />
				    </Form.Group>

				    <Form.Group className="mb-3" controlId="productPrice">
				    <Form.Label>Price </Form.Label>
				    <Form.Text className=" text-danger">
				        &nbsp;*
				    </Form.Text>
				    <InputGroup className="mb-3">
				        <InputGroup.Text>&#8369;</InputGroup.Text>
				        <Form.Control 
				        	type="number" 
				        	placeholder="Enter Price" 
				        	value = {productPrice}
				        	onChange = { e => setProductPrice(e.target.value)}
				        	required 
				        />
				      </InputGroup>
				    </Form.Group>
				
				  <div className="d-flex justify-content-end">
					  { isActive ? 
					  		<Button variant="success" type="submit" id="submitBtn" className="col-2">
					  		  Save
					  		</Button>
					  	:
						  	<Button variant="danger" type="submit" id="submitBtn" className="col-2" disabled>
						  	  Save
						  	</Button>
					  }

					<Button variant="danger" className="col-2 backBtn" onClick={() => backToProducts()}>Back</Button>
				  </div>  
				</Form>
			</Container>
	)
}