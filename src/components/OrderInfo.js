import Container from 'react-bootstrap/Container'
import { Table, thead, tbody, tr, th ,td, Button, Form } from 'react-bootstrap'
import { Link, useParams, useHistory } from 'react-router-dom'
import { Fragment, useState, useEffect,useContext } from 'react'
import UserContext from '../UserContext' 
import OrderTable from '../components/OrderTable';



export default function OrderInfo() {

	const { orderId } = useParams();
	const {user, retrieveProductDetails, productInfo} = useContext(UserContext)
	const history = useHistory()

	const [loading, setloading] = useState(true)
	const [initialRun, setInitialRun] = useState(true)

	const [count, setCount] = useState(0)

	//for table ng array
	const [OTable, SetOTable] = useState([])

	const [detail, setDetail] = useState({
		  orderId:'', 
		  userId:'', 
		  productId:'', 
		  productQty:'', 
		  productPrice:'', 
		  totalAmount:'', 
		  isPaid:'', 
		  purchasedOn:'', 
		  deliveryStatus:'', 
		  isProcessed:''
		});

	const backToOrders = () => {
		history.push("/orders")
	}

	useEffect(() => {
		if (user.isAdmin && initialRun ){
			fetch(`https://stark-refuge-17906.herokuapp.com/api/orders/${orderId}`, {
				method:'GET',
				headers: {
					"Content-Type":"application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(response => response.json())
			.then(data => {
				 let newQty =data.productQty
				 
				 setDetail({
				    orderId:data._id, 
				    userId:data.userId, 
				    productId:data.productId, 
				    productQty:data.productQty, 
				    productPrice:data.productPrice, 
				    totalAmount:data.totalAmount, 
				    isPaid:data.isPaid, 
				    purchasedOn:data.purchasedOn, 
				    deliveryStatus:data.deliveryStatus, 
				    isProcessed:data.isProcessed
			    })

				 let newOrder = []

				 // console.log(data.productId.length)
				 for (let i=0; i<data.productId.length; i++) {
				 	setCount(prevCount => prevCount + parseInt(data.productQty[i]))

				 	//find product info
	            	async function fetchName() {
	            	  const response = await fetch(`https://stark-refuge-17906.herokuapp.com/api/products/${data.productId[i]}`)
	            	  const info = await response.json();
	            	  return info;
	            	}

	            	//product info
	            	fetchName().then(pData => {
	            		// pName.push(idx+ pData.productName)
	            		let productList = {
						 	productId:data.productId[i], 
						 	productName:pData.productName,
						 	productCategory:pData.productCategory,
						 	productVolume:pData.productVolume,
						    productQty:data.productQty[i], 
						    productPrice:data.productPrice[i]
						 }
						 newOrder.push(productList)
						 // console.log(newOrder)

					 	 SetOTable(
					  	 	newOrder.map((item,idx) => {
					  	 		return(
					 				<OrderTable key={idx} orderListProp={item} />
					 			);
					 		})
					 	 )


	            	});

				 }//for loop



				 // let newPrice =data.productPrice
				 // setCount(newPrice.reduce((a,b) => {
					// 	return a+b
					// }))

			})
			setInitialRun(false)
		}
		

	},[OTable]);


	return(
		<Container>
			<div className="d-flex justify-content-between mt-5 mb-3">
 				<h1>Order Details</h1>
 				<Button variant="success" type="submit" className="newProductBtn Square text-white my-3" onClick={() => backToOrders()}>
 		      	 	 Back to Orders</Button>   		      	 			      		      	 	
	  	 	</div>

			<h3>Order ID : {orderId}</h3>
			<div className="row justify-content-between ">
				<div className="col-md-6 my-4 order-1">
					<div>
						<Form.Label className="profLabel">User ID</Form.Label>
						<Form.Label >&nbsp;:&nbsp;</Form.Label>
						<Form.Label className="profData">{detail.userId}</Form.Label>
					</div>

					<div>
						<Form.Label className="profLabel">Purchase Date</Form.Label>
						<Form.Label >&nbsp;:&nbsp;</Form.Label>
						<Form.Label className="profData">{detail.purchasedOn}</Form.Label>
					</div>

					<div>
						<Form.Label className="profLabel">Delivery Status</Form.Label>
						<Form.Label >&nbsp;:&nbsp;</Form.Label>
						<Form.Label className="profData">{detail.deliveryStatus}</Form.Label>
					</div>
				</div>

				<div className="col-md-6 my-4 order-2">	
					<div>
						<Form.Label className="profLabel">Status</Form.Label>
						<Form.Label >&nbsp;:&nbsp;</Form.Label>
				      	{(detail.isProcessed) ?
				      	 	<Form.Label className="profData">Processed</Form.Label>
				      		:
				 			<Form.Label className="profData">Open</Form.Label>
					    }
					</div>

					<div>
						<Form.Label className="profLabel">Payment</Form.Label>
						<Form.Label >&nbsp;:&nbsp;</Form.Label>
				      	{(detail.isPaid) ?
			  	      		<Form.Label className="profData">Paid</Form.Label>
			  	      		:
			  		    	<Form.Label className="profData">Pending</Form.Label>
			  	 		}
					</div>

					<div>
						<Form.Label className="profLabel">Total Amount&nbsp;&nbsp;&#8369;</Form.Label>
						<Form.Label >&nbsp;:&nbsp;</Form.Label>
						<Form.Label className="profData">{detail.totalAmount}</Form.Label>
					</div>
				</div>
			</div>

			<h3>Items: {count}</h3>
			<Table striped bordered hover size="sm" className="my-5 border border-dark">
				<thead>
				  <tr className="text-center bg-dark text-white">
				    <th>Product ID</th>
				    <th>Product Name</th>
				    <th>Product Category</th>
				    <th>Product Volume</th>
				    <th>Quantity</th>
				    <th>Price</th>
				    <th>Sub Total</th>
				  </tr>
				</thead>
			 	<tbody>
				  	{OTable}
			  	</tbody>
			</Table>

		</Container>
	)
}