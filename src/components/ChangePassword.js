import { useState, useEffect, useContext } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
import { Redirect, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'



export default function ChangePassword() {
    const { user } = useContext(UserContext)
    const history = useHistory()
    const [isActive, setIsActive] = useState(false);
    const [current, setCurrent] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');


    useEffect(() => {
        if ((current !== '' && password1 !== '' && password2 !== '') && (current !== password1) && (password1 === password2)) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    }, [current, password1, password2])


    const changePassword = () => {
        Swal.fire({
            title: 'Do you want to save the changes?',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            customClass: {
                actions: 'my-actions',
                cancelButton: 'order-1 right-gap',
                confirmButton: 'order-2',
            }
        }).then((result) => {
            if (result.isConfirmed) {
                updatePassword()
            }
        })
    }

    const handleClose = () => {
        setIsActive(false);
        setCurrent('');
        setPassword1('');
        setPassword2('');
    }

    const updatePassword = () => {
        if (user.id !== null) {
            fetch('https://stark-refuge-17906.herokuapp.com/api/users/changePassword', {
                method: 'PUT',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    currentPassword: current,
                    newPassword: password1
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data) {
                        let timerInterval
                        Swal.fire({
                            title: 'Saved',
                            icon: 'success',
                            text: 'You will be logged out and automatically redirected to the Login page.',
                            timer: 2500,
                            timerProgressBar: true,
                            didOpen: () => {
                                Swal.showLoading()
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then(result => {
                            handleClose()
                            history.push("/logout")
                        })
                    }
                    else {
                        Swal.fire('Authentication Failed', 'Please check the provided password', 'error')
                    }
                })
        }
    }
    
    return (
        (localStorage.getItem('token') === null) ?
            <Redirect to="/" />
            :
            <Container>
                <h1>Change Password</h1>

                <Form.Group className="mb-3">
                    <Form.Text className=" text-secondary">New Password must be different from current password.</Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="current">
                    <Form.Label>Current Password</Form.Label>
                    <Form.Text className=" text-danger">&nbsp;*</Form.Text>
                    <Form.Control
                        type="password"
                        placeholder="Current Password"
                        value={current}
                        onChange={e => setCurrent(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password1">
                    <Form.Label>New Password</Form.Label>
                    <Form.Text className=" text-danger">&nbsp;*</Form.Text>
                    <Form.Control
                        type="password"
                        placeholder="Current Password"
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Retype New Password</Form.Label>
                    <Form.Text className=" text-danger">&nbsp;*</Form.Text>
                    <Form.Control
                        type="password"
                        placeholder="Retype New Password"
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>

                {isActive ?
                    <Button variant="success" onClick={() => changePassword()}>
                        Save Changes
                    </Button>
                    :
                    <Button variant="secondary" disabled>
                        Save Changes
                    </Button>
                }
            </Container>
    )
}