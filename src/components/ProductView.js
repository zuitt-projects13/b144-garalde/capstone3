import { useState, useEffect,useContext } from 'react'
import UserContext from '../UserContext'  

import { Container, Card, Button, Form } from 'react-bootstrap'
import { Redirect, useParams, useHistory} from 'react-router-dom'

import Swal from 'sweetalert2'



export default function ProductView() {

	const [productName, setProductName] = useState('');
	const [productCategory, setProductCategory] = useState('');
	const [productVolume, setProductVolume] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState('');
	const [productQty, setProductQty] = useState(1);

	const history = useHistory()
	const { productId } = useParams();
	const { user, setCartBadge, retrieveProductDetails, productInfo } = useContext(UserContext)

	useEffect(() => {
		if(productInfo.productName === ''|| productInfo.pID !== productId){
			retrieveProductDetails(productId)
		}
		setProductName(productInfo.productName);
		setProductCategory(productInfo.productCategory);
		setProductVolume(productInfo.productVolume);
		setProductDescription(productInfo.productDescription);
		setProductPrice(productInfo.productPrice);

	},[productId,productInfo]);

	const addToCart = () => {

		fetch('https://stark-refuge-17906.herokuapp.com/api/users/addToCart',{
			method:'POST',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId : productId,
				productQty : productQty
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data === false){
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "An error has been encountered while saving."
				})
			}
			else {
				setCartBadge(prevCount => prevCount + 1)
				Swal.fire({
					icon: 'success',
					title: 'Successfully Added Product',
					showConfirmButton: false,
					timer: 1000
				})

			}
		})
	}

	const backToProducts = () => {

		history.push("/products")
	}


	const setQty = (task) => {
		if (task === "decrease"){
			if(productQty !== 1) {
				setProductQty(prevCount => prevCount - 1)
			}
			
		} else {
			setProductQty(prevCount => prevCount + 1)
		}
	}

	return (
		<Container>
			<h1>View Product</h1>

			<Container className="d-flex flex-column align-items-center">

				<Card className="cardHighlight w-90" >

					<div className="row">
						<div className="col-md-6 my-4 order-1 px-5">

							<Card.Img  className="cardImage" variant="top" src="https://images.pexels.com/photos/3989394/pexels-photo-3989394.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" />

						</div>
						<div className="col-md-6 my-4 order-2">
							<Card.Title className="block mb-3">{productName}</Card.Title>
							<Card.Subtitle>Category:</Card.Subtitle>
							<Card.Text>{productCategory}</Card.Text>

							<Card.Subtitle>Volume:</Card.Subtitle>
							<Card.Text>{productVolume}</Card.Text>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{productDescription}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>&#8369; {productPrice}</Card.Text>

							<div className="mb-2">
								<Button variant="secondary" type="submit" className="square text-white" onClick={() => setQty("decrease")}>
									<strong>-</strong>
								</Button>
								<Form.Label className="block mx-3"><strong>{productQty}</strong></Form.Label> 	
								<Button variant="secondary" type="submit" className="Square text-white" onClick={() => setQty("increase")}>
									<strong>+</strong>
								</Button>
								{(user.id === null || user.isAdmin === true) ?
									<Button variant="primary" hidden disabled>Add to cart</Button>
									:
									<Button variant="primary" className="mx-4" onClick={() => addToCart()}>Add to cart</Button>
								}	
							</div>
							<div className="d-flex justify-content-end px-3">
								<Button variant="success" onClick={() => backToProducts()}>Back</Button>
							</div>
						</div>
					</div>
				</Card>
			</Container>

		</Container>

		
		)
}