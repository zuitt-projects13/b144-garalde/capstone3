import PropTypes from 'prop-types'
import { tr, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Container from 'react-bootstrap/Container'
import Swal from 'sweetalert2'
import { Fragment } from 'react';
import { useState, useEffect } from 'react'

export default function AllProducts({productProp}) {

	const { _id, productName, productCategory, productVolume, productDescription, productPrice, isActive } = productProp


	const deactivate = (productId) => {

		fetch(`https://stark-refuge-17906.herokuapp.com/api/products/${productId}/archive`, {
			method:'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if (data === false){
        		Swal.fire({
                  title: "Error",
                  icon: "error",
                  text: "An error has been encountered while saving."
                })
			}
			else {
				Swal.fire({
				  icon: 'success',
				  title: 'Deactivated',
				  showConfirmButton: false,
				  timer: 1000
				})

			}
		})
	}

	const activate = (productId) => {
		fetch(`https://stark-refuge-17906.herokuapp.com/api/products/${productId}/activate`, {
			method:'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			if (data === false){
        		Swal.fire({
                  title: "Error",
                  icon: "error",
                  text: "An error has been encountered while saving."
                })
			}
			else {
				Swal.fire({
				  icon: 'success',
				  title: 'Activated',
				  showConfirmButton: false,
				  timer: 1000
				})

			}
		})
	}


	return (
	    <tr>
	      	<td>{productName}</td>
	      	<td>{productCategory}</td>
	      	<td>{productVolume}</td>
	      	<td>{productDescription}</td>
	      	<td>&#8369; {productPrice}</td>

	      	{(isActive) ?
	      		<td><strong>Available</strong></td>
	      		:
		    	<td className="text-danger"><strong>Inactive</strong></td>
	 		}
	      	{(isActive) ?
	      	 	<td><Button variant="secondary" type="submit" className="w-100 rounded-pill text-white my-1" onClick={() => deactivate(_id)}>
		      	 	 Deactivate
		      	 	</Button></td>
	      		:
		      	<td><Button variant="warning" type="submit" className="w-100 rounded-pill text-white my-1" onClick={() => activate(_id)}>
		      	 	 Reactivate
		      	 	</Button></td>
		 	 	}
     		<td className="d-flex flex-column align-items-center px-2">
	     		<Link className="btn btn-info w-100 rounded-pill text-white my-1"  to={`/products/${_id}/update`}>Update</Link>
	 	 	</td>
	 	 	
	    </tr>
	)
}