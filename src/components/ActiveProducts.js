import { Container, Card, Row, Col, Button } from 'react-bootstrap'
import { useState, useEffect,useContext } from 'react'
import { useHistory} from 'react-router-dom'
import UserContext from '../UserContext'  

export default function ActiveProducts({productProp}) {

	const { _id, productName, productCategory, productVolume, productDescription, productPrice } = productProp

	const history = useHistory()

	// const productImg = "https://images.pexels.com/photos/3989394/pexels-photo-3989394.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260"
	const productImg = require("../images/logo.png")

	const viewProduct = () => {
		history.push(`/products/${_id}`)
	}

	return (
	      

      <Card className="cardActiveProduct col-12 col-md-3" onClick={() => viewProduct()}>
	        <Card.Img className="cardActiveImg" variant="top" src={productImg} />
	        <Card.Body>
		        <Card.Title>{productName}&nbsp;&nbsp;|&nbsp;&nbsp;<i>{productCategory}</i></Card.Title>
	        </Card.Body>
        </Card>


	     
	) //return
}