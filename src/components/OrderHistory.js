import Container from 'react-bootstrap/Container'
import UserContext from '../UserContext'  
import ShowCart from '../components/ShowCart';
import { Fragment, useState, useEffect, useContext, useReducer } from 'react';
import { Redirect } from 'react-router-dom'
import { Table, thead, tbody, tr, th } from 'react-bootstrap'



export default function OrderHistory() {
	const { user, cart, setCart } = useContext(UserContext)
	const [loading, setloading] = useState(true)
	const [initialRun, setInitialRun] = useState(true)
	const [total, setTotal] = useState(0)
	const [count, setCount] = useState(0)
	let paidItems = []

	useEffect(() => {

		// if (user.id !== null && user.isAdmin === false && initialRun === true){
		if (initialRun === true){
			setInitialRun(false) 
	      	fetch('https://stark-refuge-17906.herokuapp.com/api/users/details', {
  	            headers: {
  	            Authorization :`Bearer ${localStorage.getItem('token')}`
  	            }
  	        })
  	        .then(response => response.json())
  	        .then(data => {
  	        	data=data.cart
  	        	let data2=data
  	        	console.log(data.length)
				data2.forEach((item) => {
  	            	if(item.isCheckedOut === true){
	  	            	paidItems.push(item._id)
  	            	}
  	            })
  	            setCount(paidItems.length)
  	            setloading(false)

  	            let newOrder = []

  	            

				 // console.log(data.productId.length)
				 for (let i=0; i<data.length; i++) {

				 	//find product info
	            	async function fetchName() {
	            	  const response = await fetch(`https://stark-refuge-17906.herokuapp.com/api/products/${data[i].productId}`)
	            	  const info = await response.json();
	            	  return info;
	            	}

	            	console.log(`${i} ${data[i]}`)
	            	//product info
	            	fetchName().then(pData => {
	            		// console.log(pData)//productInfo

	            		// pName.push(idx+ pData.productName)
	            		let productList = {
						 	_id:data[i]._id, 
						 	productId:data[i].productId, 
						 	orderId:data[i].orderId, 
						 	productName:pData.productName,
						 	productCategory:pData.productCategory,
						 	productVolume:pData.productVolume,
						    productQty:data[i].productQty, 
						    productPrice:data[i].productPrice,
						    isCheckedOut:data[i].isCheckedOut,
						    isProcessed:data[i].isProcessed
						 }
	            		console.log(productList)
						 newOrder.push(productList)
						 console.log(newOrder)

					 	setCart(
			 	 			newOrder.map((item,index) => {
			 	 			if(item.isCheckedOut === true){
								return(
									<ShowCart key={item._id} cartProp={item} idx={index}/>
								);
							}
						})
				)


	            	});

				 }//for loop

				 

  	            /*setCart(
			 	 	data.map((item,index) => {
			 	 		if(item.isCheckedOut === true){
							return(
								<ShowCart key={item._id} cartProp={item} idx={index}/>
							);
						}
					})
				)*/



  	        })
  	       
	    }
	},[cart])	


	return(
		(user.id === null || user.isAdmin === true) ?
            <Redirect to="/" />
	        :
			<Container>
				<h1>Order History</h1>
				<br/>
				<p>Below are your previously placed orders.</p>
				<p>Items are either pending for seller processing or already processed by seller.</p>
				{loading ?
					"Loading..."
					:
					<Fragment>
					{(count === 0) ?
						<h3>Your history is empty.</h3>
						:
						<Fragment>
							<Table striped bordered hover size="sm" className="my-5 border border-dark">
								<thead>
								  <tr className="text-center bg-dark text-white">
								    <th>Live View</th>
								    <th>Product Name</th>
								    <th>Product Category</th>
								    <th>Product Volume</th>
								    <th>Price </th>
								    <th>Quantity</th>
								    <th>SubTotal</th>
								    <th>Status</th>
								  </tr>
								</thead>
								  <tbody>
								  	{cart}
								  </tbody>
							</Table>
						</Fragment>
					}
					</Fragment>
				}		
			</Container>
	)
}