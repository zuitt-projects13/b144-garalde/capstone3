//search bar
import React from "react";
import { Fragment } from 'react';
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'
import { Link, NavLink, useHistory } from 'react-router-dom'

//import necessary react-bootstrap components
import { Form, Button, FormControl } from 'react-bootstrap'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'

import { Badge, Avatar } from 'antd';
import { ShoppingCartOutlined } from '@ant-design/icons'

export default function AppNavbar() {
	const { user, cartBadge, search, setSearch, setSource } = useContext(UserContext)
	const history = useHistory()

	const logoIcon = require("../images/hermosalogo.png")
	const userIcon = require("../images/user.png")


	const searchProduct = (lookFor) => {
		setSearch(lookFor)
		if(lookFor === ''){
			setSource("All")
		} else {
			setSource("Search")
		}
	
		history.push("/products")
	}

	const goToProducts = () => {
		setSearch('')
		setSource("All")
	}

	return (
		<Navbar className="navbar" expand="lg" fixed="top" sticky="top">
			<Container>
				<Navbar.Brand as={Link} to="/"><img src={logoIcon} className="iconLogo" /></Navbar.Brand>

				<Form className="d-flex">
					<FormControl
						type="search"
						placeholder="Search"
						className="me-2"
						aria-label="Search"
						value={search}
						onChange={e => searchProduct(e.target.value)}
					/>
					<FontAwesomeIcon onClick={() => searchProduct(search)} className="navIcon" icon={faSearch}></FontAwesomeIcon>
				</Form>

				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="d-flex justify-content-end w-100">

						<Nav.Link as={NavLink} to="/products" onClick={() => goToProducts()} className="navText">Products</Nav.Link>

						{(user.id !== null) ?
							<Fragment>
								{(user.isAdmin === true) ?
									<Nav.Link as={NavLink} to="/orders" className="navText" >Orders</Nav.Link>
									:
									<Nav.Link as={NavLink} to="/cart" className="navBadge" >
										<Badge count={cartBadge} showZero  style={{ backgroundColor: '#cc8b65', borderRadius:'50px', paddingLeft:'5px', paddingRight:'5px'}} >
										<ShoppingCartOutlined className="navIcon" />
											<Avatar size="large" />
										</Badge>
									</Nav.Link>
								}

								<Nav.Link as={NavLink} to="/profile" className="navText"> Hello {user.firstName}!</Nav.Link>
								<Nav.Link as={NavLink} to="/logout" >
									<FontAwesomeIcon className="navIcon" icon={faSignOutAlt}></FontAwesomeIcon>
								</Nav.Link>
							</Fragment>
							:
							<Fragment>
								<Nav.Link as={NavLink} to="/login" className="navText">Login</Nav.Link>
								<Nav.Link as={NavLink} to="/register" className="navText">Register</Nav.Link>
							</Fragment>
						}

					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}
