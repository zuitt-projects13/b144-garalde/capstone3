import Container from 'react-bootstrap/Container'
import UserContext from '../UserContext'  

import Swal from 'sweetalert2'
import { Redirect, Link, useHistory } from 'react-router-dom'

import { Fragment, useEffect, useState, useContext } from 'react';
import ShowCart from '../components/ShowCart';
import { Table, thead, tbody, tr, th, Form, Button} from 'react-bootstrap'


export default function Cart() {

	const history = useHistory()

	const { retrieveProductDetails, productInfo, setProductInfo, updateCart, refreshCart, setCartBadge } = useContext(UserContext)
	const { user, cart, setCart } = useContext(UserContext)
	const [loading, setloading] = useState(true)
	const [loadName, setLoadName] = useState([])
	const [cartName, setCartName] = useState([])

	const [total, setTotal] = useState(0)
	const [count, setCount] = useState(0)
	let prices = []
	let pName = []


	useEffect(() => { 
		updateCart(true)
	},[])


	useEffect(() => {
		if (refreshCart===true){
			fetchCart()
		}
	},[cart,refreshCart])	


	const fetchCart = () => {
		if (user.id !== null && user.isAdmin === false){
	      	fetch('https://stark-refuge-17906.herokuapp.com/api/users/details', {
  	            headers: {
  	            Authorization :`Bearer ${localStorage.getItem('token')}`
  	            }
  	        })
  	        .then(response => response.json())
  	        .then(data => {
  	        	data=data.cart
  	        	let data2=data
  	        	countTotal(data2)
  	        	cartContent(data)
  	        })
	    }
	}


	const cartContent = (data) => {
		setCart(
	 	 	data.map((item,index) => {
	 	 		if(item.isCheckedOut === false){

					return(
						<ShowCart key={item._id} cartProp={item} idx={index}/>
					);
				}

			})
		)

		setloading(false)
		updateCart(false)
	}

	

	const countTotal = (data2) => {
    	//get prices
        data2.forEach((item,idx) => {
        	if(item.isCheckedOut === false){
            	prices.push(item.productPrice*item.productQty)
        	}
        })

        //#of items
        setCount(prices.length)
        setCartBadge(prices.length)
        // setCartName(pName)

            //get total price
            if(prices.length !== 0){
			setTotal(prices.reduce((a,b) => {
				return a+b
			}))
		}
	}

	const checkOut = () => {
		if (user.id !== null && user.isAdmin === false){
			// setTotal(0)	
	      	fetch('https://stark-refuge-17906.herokuapp.com/api/users/checkout', {
	      		method:'POST',
  	            headers: {
  	            Authorization :`Bearer ${localStorage.getItem('token')}`
  	            }
  	        })
  	        .then(response => response.json())
  	        .then(data => {
  	        
  	        	if (data === false){
	        		Swal.fire({
	                  title: "Error",
	                  icon: "error",
	                  text: "An error has been encountered while saving.",
	                  showConfirmButton: false,
	                  timer: 1000
	                })
				} else {
					setTotal(0)
	        		Swal.fire({
	                  title: "Check Out Successful",
	                  icon: "success",
	                  text: "Items has been checked out",
	                  showConfirmButton: false,
	                  timer: 1000
	                })

	        		
	        		viewHistory()
				}
			})	
				
  	    }
	}
	
	const viewHistory = () => {
		setTotal(0)
		setCount(0)
		setCart([])
		prices = []
		history.push("/orderHistory")
	}


	return (
		(user.id === null || user.isAdmin === true) ?
            <Redirect to="/" />
        :
			<Container>
				<div className="d-flex justify-content-between mt-5">
				<h1>Cart</h1>
				
				<Link to="/orderHistory" onClick={() => viewHistory()}>View Order History</Link>	      		      	 			      		      	 	
				</div>		      		      	 	
		      	{(loading) ?
					"Loading..."
					:	      	 	
					<Fragment>
					{(total === 0) ?
						<h3>Your cart is empty.</h3>
						:
						<Fragment>
						<h3 className="mt-5 mb-0">Items: {count}</h3>
						<Table striped bordered hover size="sm" className="my-5 border border-dark">
							<thead>
							  <tr className="text-center bg-dark text-white">
							    <th>Live View</th>
							    <th>Product Name</th>
							    <th>Product Category</th>
							    <th>Product Volume</th>
							    <th>Price </th>
							    <th>Quantity</th>
							    <th>SubTotal</th>
							    <th>Remove</th>
							  </tr>
							</thead>
							  <tbody>
							  	{cart}
							  </tbody>
						</Table>
						<div className="d-flex flex-column align-items-end">
					      	<Form.Label className="cartTotal"><strong>Total Amount: &#8369; {total}</strong></Form.Label>
					      	<br/> 	
					      	<Button variant="primary" type="submit" className="Square text-white cartTotal" onClick={() => checkOut()}>
				      		      	 	 Check Out
				      		      	 	</Button>
						</div>			      		      	 	
			      		</Fragment>      	 	
			      	}
			      	</Fragment>	      	 	
			    }
			</Container>

	)
}