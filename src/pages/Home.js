import Container from 'react-bootstrap/Container'
import Landing from '../components/Landing'

export default function Home() {
	return (
		<div className='landingContainer'>
			<Landing />
		</div>
	)
}