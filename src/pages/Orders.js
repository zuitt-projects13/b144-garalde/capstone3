import UserContext from '../UserContext' 
import { Fragment, useEffect, useState, useContext } from 'react';
import { Redirect, Link, useHistory } from 'react-router-dom'
import Container from 'react-bootstrap/Container'
import { Table, thead, tbody, tr, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

import OrderData from '../components/OrderData';
import AllProducts from '../components/AllProducts';

export default function Orders() {

	const {user, orders, setOrders} = useContext(UserContext)
	const [loading, setloading] = useState(true)
	const [allBtn, setAllBtn] = useState(true)
	const [allColor, setAllColor] = useState("outline-success")

	const [openBtn, setOpenBtn] = useState(false)
	const [openColor, setOpenColor] = useState("outline-success")

	const [activeBtn, setActiveBtn] = useState(false)
	const [activeColor, setActiveColor] = useState("outline-success")

	const [selected, setSelected]=useState("All")
	const [count, setCount]=useState(0)

	const [initialRun, setInitialRun] = useState(true)
	let counter = []


	const timer = () => {
		let timerInterval
		Swal.fire({
		  title: 'Loading...',
		  timer: 1600,
		  timerProgressBar: true,
		  didOpen: () => {
		    Swal.showLoading()
		  },
		  willClose: () => {
		    clearInterval(timerInterval)
		  }
		})
	}

	const resetBtn = () => {
		timer()	

		setAllBtn(false)
		setOpenBtn(false)
		setActiveBtn(false)

		setAllColor("outline-success")
		setOpenColor("outline-success")
		setActiveColor("outline-success")
	}


	const allOrders = (data) => {
		setloading(true)
		setCount(0)
		resetBtn()
		setAllBtn(true)
		setAllColor("success")
		setSelected("All")
		setOrders([])
	}

	const openOrders = (data) => {
		setloading(true)
		setCount(0)
		resetBtn()
		setOpenBtn(true)
		setOpenColor("success")
		setSelected("Open")
		setOrders([])
	}


	const processedOrders = () => {
		setloading(true)
		setCount(0)
		resetBtn()
		setActiveBtn(true)
		setActiveColor("success")
		setSelected("Processed")
		setOrders([])
	}


	const getOrders = () => {
		if (user.isAdmin === true) {
			fetch('https://stark-refuge-17906.herokuapp.com/api/orders/allOrders', {
  	            headers: {
  	            Authorization :`Bearer ${localStorage.getItem('token')}`
  	            }
  	        })
  	        .then(response => response.json())
  	        .then(data => {

    	 	 	data.forEach(orderItem => {
    	 	 		if (selected === "All"){
    		 	 		counter.push(orderItem._id)
    	 	 		}
    	 	 		else if (selected === "Open"){
    	 	 			if(!orderItem.isProcessed){
    	 	 				counter.push(orderItem._id)
    	 	 			}
    	 	 		}
    	 	 		else if (orderItem.isProcessed === true){
    	 	 			counter.push(orderItem._id)
    	 	 		}
    				
    			})

    	 	 	setCount(counter.length)

				setOrders(
			 	 	data.map(orderItem => {
			 	 		if (selected === "All"){
				 	 		return(
								<OrderData key={orderItem._id} orderProp={orderItem} />
							);
			 	 		}
			 	 		else if (selected === "Open"){
			 	 			if(!orderItem.isProcessed){
				 	 			return(
									<OrderData key={orderItem._id} orderProp={orderItem} />
								);
			 	 			}
			 	 		}
			 	 		else if (orderItem.isProcessed === true){
			 	 			return(
								<OrderData key={orderItem._id} orderProp={orderItem} />
							);
			 	 		}
						
					})
				)

				setloading(false)
			})
		}
	}

	useEffect(() => {
		getOrders()
		if (initialRun ) {
			setInitialRun(false)
			timer()	
		}
	},[orders])	




	return (
		(user.id === null || user.isAdmin === false) ?
            <Redirect to="/" />
        :
		<Container>
			<h1>Orders</h1>
			<h4>{selected} items: {count}</h4>
			<div className="d-flex justify-content-center text-white mt-5 w-100">
				<Button variant={allColor} className="w-50 rounded-pill mx-1" active={allBtn} onClick={() => allOrders()}>
				      	 	 All Orders
				      	 	</Button>

				<Button variant={openColor} className="w-50 rounded-pill mx-1" active={openBtn} onClick={() => openOrders()}>
				      	 	 Open
				      	 	</Button>
				<Button variant={activeColor} className="w-50 rounded-pill mx-1" active={activeBtn} onClick={() => processedOrders()}>
				      	 	 Processed
				      	 	</Button>
			</div> 

			{(loading) ?
					"Loading..."
					:	
					<Fragment>
						<Table striped bordered hover size="sm" className="my-5 border border-dark">
							<thead>
							  <tr className="text-center bg-dark text-white">
							    <th>Info</th>
							    <th>OrderID</th>
							    <th>Total Amount</th>
							    <th>Payment</th>
							    <th>Delivery</th>
							    <th>Status</th>
							    <th>Update</th>
							  </tr>
							</thead>
							  <tbody>
							  	{orders}
							  </tbody>
						</Table>
					</Fragment>
			}
		</Container>
	)
}