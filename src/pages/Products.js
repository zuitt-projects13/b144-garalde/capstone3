import { Fragment, useEffect, useState, useContext } from 'react';
import AllProducts from '../components/AllProducts';
import ActiveProducts from '../components/ActiveProducts';
import { Table, thead, tbody, tr, th, td, Button } from 'react-bootstrap'
import { Container, Row, Col } from 'react-bootstrap'
import UserContext from '../UserContext'
import { useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Products() {
	const { products, setProduct, user, search, source } = useContext(UserContext)
	const history = useHistory()
	const [loading, setloading] = useState(true)
	const [initialRun, setInitialRun] = useState(true)

	const [count, setCount] = useState(0)
	let items = []

	const timer = () => {
		let timerInterval
		Swal.fire({
			title: 'Loading...',
			timer: 1000,
			timerProgressBar: true,
			didOpen: () => {
				Swal.showLoading()
			},
			willClose: () => {
				clearInterval(timerInterval)
			}
		})
	}

	//all products admin access
	useEffect(() => {
		if (user.isAdmin === true) {
			fetch("https://stark-refuge-17906.herokuapp.com/api/products/allProducts", {
				method: 'GET',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
				.then(response => response.json())
				.then(data => {

					setProduct(
							data.filter((value) => {
								if (search===""){
									return value
								}else if (
									value.productName.toLowerCase().includes(search.toLowerCase()) ||
									value.productCategory.toLowerCase().includes(search.toLowerCase()) ||
									value.productDescription.toLowerCase().includes(search.toLowerCase())
									){
									return value
								}
							})
							.map(product => {
								items.push(product._id)
								return (
									<AllProducts key={product._id} productProp={product} />
								);
							})
						)

					setloading(false)
					setCount(items.length)
				})
		}

		if (initialRun) {
			setInitialRun(false)
			timer()
		}

	}, [products,search]);

	//active products user access
	useEffect(() => {
		if (user.isAdmin !== true) {
			fetch("https://stark-refuge-17906.herokuapp.com/api/products/active", {
				method: 'GET',
				headers: {
					"Content-Type": "application/json",
				}
			})
				.then(response => response.json())
				.then(data => {
					setProduct(
						data.filter((value) => {
							if (search===""){
								return value
							}else if (
								value.productName.toLowerCase().includes(search.toLowerCase()) ||
								value.productCategory.toLowerCase().includes(search.toLowerCase()) ||
								value.productDescription.toLowerCase().includes(search.toLowerCase())
								){
								return value
							}
						})
						.map(product => {
							items.push(product._id)
							return (
								<ActiveProducts key={product._id} productProp={product} />
							);
						})
					)
					setloading(false)
					setCount(items.length)
				})
		}
		if (initialRun) {
			setInitialRun(false)
			timer()
		}
	}, [search])


	const createProduct = () => {
		history.push("/createProduct")
	}

	return (
		(loading) ?
			"Loading..."
			:
			<Fragment>
				
				{(user.isAdmin === true) ?
					<Container>
						<div className="d-flex justify-content-between mt-5">
							<h1>All Products</h1>
							<Button variant="success" type="submit" className="newProductBtn Square text-white my-3" onClick={() => createProduct()}>
								New Product</Button>
						</div>
						<h5>{source} Items:{count}</h5>

						<Table striped bordered hover size="sm" className="my-5 border border-dark">
							<thead>
								<tr className="text-center bg-dark text-white">
									<th>Product</th>
									<th>Category</th>
									<th>Volume</th>
									<th>Description</th>
									<th>Price</th>
									<th>Availability</th>
									<th>Action</th>
									<th>Update</th>
								</tr>
							</thead>
							<tbody>
								{products}
							</tbody>
						</Table>
					</Container>
					:
					<Container className="w-100 pt-2">
						<h1>Products</h1>
						<h5>{source} Items:{count}</h5>
						<div className="card-deck d-flex flex-row flex-wrap mt-4">
							{products}
						</div>
					</Container>
				}
			</Fragment>

	)
}