import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'
import { Container, Form, Button } from 'react-bootstrap'
import { Redirect, useHistory, Link } from 'react-router-dom'


export default function Profile() {
	const { user } = useContext(UserContext)
	const history = useHistory()

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [age, setAge] = useState('');
	const [gender, setGender] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [deliveryAddress, setAddress] = useState('');

	// const userIcon = require("../images/logo.png")
	const userIcon = "https://images.pexels.com/photos/1214205/pexels-photo-1214205.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"


	useEffect(() => {
		if (user.id !== null) {
			fetch('https://stark-refuge-17906.herokuapp.com/api/users/details', {
				headers: {
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
				.then(response => response.json())
				.then(data => {
					setFirstName(data.firstName);
					setLastName(data.lastName);
					setAge(data.age);
					setGender(data.gender);
					setMobileNo(data.mobileNo);
					setAddress(data.deliveryAddress);
				})
		}
	}, [])

	const changePassword = () => {
		history.push("/profile/changePassword")
	}
	const updateProfile = () => {
		history.push("/profile/updateInfo")
	}

	const OrderHistory = () => {
		history.push("/orderHistory")
	}

	return (
		(user.id === null) ?
			<Redirect to="/" />
			:
			<Container>
				<div className="d-flex justify-content-between mt-5">
					<h1>Profile</h1>

					{(user.isAdmin === true) ?
						<Link to="/orderHistory" hidden>View Order History</Link>
						:

						<Link to="/orderHistory" >View Order History</Link>
					}
				</div>

				<Container className="">
					<div className="row justify-content-between ">
						<div className="col-md-6 my-4 px-5 order-1 order-md-2">
							<div className="" id="mypic">
								<img src={userIcon} className="img-fluid rounded-circle userPhoto" />
							</div>
						</div>
						<div className="col-md-6 my-4 order-1 profDiv ">
							<div>
								<Form.Label className="profLabel">First Name</Form.Label>
								<Form.Label >&nbsp;:&nbsp;</Form.Label>
								<Form.Label className="profData">{firstName}</Form.Label>
							</div>

							<br />
							<div>
								<Form.Label className="profLabel">Last Name</Form.Label>
								<Form.Label >&nbsp;:&nbsp;</Form.Label>
								<Form.Label className="profData">{lastName}</Form.Label>
							</div>

							<br />
							<div>
								<Form.Label className="profLabel">Age</Form.Label>
								<Form.Label >&nbsp;:&nbsp;</Form.Label>
								<Form.Label className="profData">{age}</Form.Label>
							</div>

							<br />
							<div>
								<Form.Label className="profLabel">Gender</Form.Label>
								<Form.Label >&nbsp;:&nbsp;</Form.Label>
								<Form.Label className="profData">{gender}</Form.Label>
							</div>

							<br />
							<div>
								<Form.Label className="profLabel">Mobile Number</Form.Label>
								<Form.Label >&nbsp;:&nbsp;</Form.Label>
								<Form.Label className="profData">{mobileNo}</Form.Label>
							</div>

							<br />
							<div>
								<Form.Label className="profLabel">Delivery Address</Form.Label>
								<Form.Label >&nbsp;:&nbsp;</Form.Label>
								<Form.Label className="profData">{deliveryAddress}</Form.Label>
							</div>
							<div className="d-flex justify-content-between mt-5">
								<Form.Label onClick={() => changePassword()} className="likeLink">Change Password</Form.Label>
								<Form.Label onClick={() => updateProfile()} className="likeLink">Update Profile</Form.Label>
							</div>

						</div>
					</div>
				</Container>
			</Container>
	)
}