import Container from 'react-bootstrap/Container'
import {Form, Button } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { useState, useEffect,useContext } from 'react'
import UserContext from '../UserContext'  
import { Redirect  } from 'react-router-dom'


export default function Logout() {

	

	const {unsetUser,user, setUser} = useContext(UserContext);

	

	useEffect(() => {
		//set the user state back to its original value
		if (user.id !== null) {
			unsetUser()

			setUser({
			    id: null,
			    isAdmin: null
			});
		}
	})


	return(
		(user.id === null) ?
            <Redirect to="/" />
        :

		<Redirect to='/login' />
	)
}